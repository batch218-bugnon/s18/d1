// console.log("Hello World"); 

/* function printInfo() {
	let nickname = prompt ("Enter your nickname: ");
	console.log ("Hi, " + nickname);	// body...
}

printInfo();
*/

					//firstName is parameter
function printName(firstName){
	console.log("My name is " + firstName);
}

printName("Juana"); // Juana is argument 
printName("Iris");
printName("Ishi");

//  Now we have a reusable function/reusable task but could have diff output based on what value to process, with the help of paramters and arguments 

// [SECTION] Parameters and Arguments 

// Paramenter 
	// "firstName" is called a parameter
	//  a "parameter" acts as named variable/container that exists only inside a function
	// It is used to strore information that is provided to a function when it is called/invoke


// Argument 
	// "Juana", "Iris", "Ishi" - the information/data provided directly into the function is called "argument"
	// Value passed when invoking a function are called arguments
	// These areguments are then stored as the parameter within the function 


let sampleVariable = "Inday";

printName(sampleVariable);
// variables can also be passed as an argument

// ---------------------------------------

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isdivisibilityBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8? ");
	console.log(isdivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


// [SECTION] Function as argument 
	// Function parameter can also accept functions as arguments
	// Some complex functions uses other functions to perform more complicated results.


	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");

	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

invokeFunction(argumentFunction);
console.log(argumentFunction);


// ------------------------------------------

// [SECTION] Using Multiple Parameters 

	function createFullName(firstName, middleName, lastName){
	console.log("My full name is " + firstName + " " + middleName + " " + lastName);
}
	createFullName("Iris", "Bustamente", "Bugnon");


	// Using variables as an argument 
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);




	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference: " + (numA - numB));
	}

	getDifferenceOf8Minus4(8,4);
	// getDifferenceOf8Minus4(4,8); // this will result to logical error	



// [SECTION] Return Statement 
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called
function returnFullName(firstName, middleName, lastName) {
	// return firstName + " " + middleName + " " + lastName;

	// we could also create a variable inside the function to create the result and return the variable instead
	let fullName = firstName + " " + middleName + " " + lastName; return fullName;

	// This line of code will not be printed
	console.log("This is printed inside a function");

}

let completeName = returnFullName("Paul", "Smith", "Jordan");
console.log(completeName);

console.log("I am " + completeName);




function printPlayerInfo(userName, level, job){
	console.log("Username: " + userName);
	console.log("Level: " + level);
	console.log("Job: " + job);
}


let user1 = printPlayerInfo("boxzMapagmahal", "Senior", "Programmer"); 
console.log(user1); // returns undefined


